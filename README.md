
Work towards creating a repnet HTTP reverse proxy.

See http://phoenixkahlo.com/hubs/repnet

Demo
---

You'll need 5 terminal windows simultaneously, and you'll wanna start by
initializing the following env vars in all of them:

```sh
export RUST_LOG=warn,repnet_http_proxy=trace,repnet_account_server=trace,repnet_account_server_core=trace,repnet_account_client=trace
export ACCOUNT_SERVER_PORT=6725
export OUTER_HTTP_SERVER_PORT=6500
export INNER_HTTP_SERVER_PORT=6501
export REPNET_FOOBAR=42
```

#### Terminal 1: inner web server

```sh
python3 -m http.server $INNER_HTTP_SERVER_PORT
```

> You may test it by running in another window:
  
  ```sh
  curl http://localhost:$INNER_HTTP_SERVER_PORT/README.md
  ```

#### Terminal 2: account server

```sh
cargo run --bin repnet-account-server -- 127.0.0.1:$ACCOUNT_SERVER_PORT
```

#### Terminal 3: reverse proxy

```sh
cargo run --bin repnet-http-proxy -- 127.0.0.1:$OUTER_HTTP_SERVER_PORT 127.0.0.1:$INNER_HTTP_SERVER_PORT http://127.0.0.1:$ACCOUNT_SERVER_PORT
```

#### Terminal 4: (for your viewing pleasure) watch account priority

```sh
wscat --connect ws://localhost:$ACCOUNT_SERVER_PORT/peer/$REPNET_FOOBAR/priority/watch
```

> At the beginning it should look like this:
  
  ```
  Connected (press CTRL+C to quit)
  < 0
  ```

#### Terminal 5: testing commands

1. Credit the account

   ```sh
   curl -X POST -H "Content-Type: application/json" -d "100" http://localhost:$ACCOUNT_SERVER_PORT/peer/$REPNET_FOOBAR/credit/inet-egress-bytes
   ```

   > You should notice a new line appear in terminal 4:

     ```
     Connected (press CTRL+C to quit)
     < 0
     < 1
     ```
2. Make a request on-credit:

   ```sh
   curl -H "Repnet-Foobar: $REPNET_FOOBAR" http://localhost:6500/README.md
   ```

   It should print this readme. You can do this repeatedly, and as you do, you
   should see each time a new line appear in terminal 4 (exact numbers may
   vary):

   ```
   Connected (press CTRL+C to quit)
   < 0
   < 1
   < 0.4444444444444444
   < 0.25
   < 0.16
   < 0.1111111111111111
   < 0.08163265306122448
   ```
