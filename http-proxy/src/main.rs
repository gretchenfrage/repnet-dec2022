
use repnet_http_proxy::proxy;
use repnet_account_client::AccountClient;
use std::{
    net::ToSocketAddrs,
    env::args,
};
use anyhow::{
    Result,
    bail,
};
use tracing_subscriber::{
    prelude::*,
    Registry,
    EnvFilter,
};


#[tokio::main]
async fn main() -> Result<()> {
    let args = args().collect::<Vec<_>>();
    if args.len() != 4 {
        eprintln!("Usage: [binary] [bind to] [proxy to] [account server]");
        bail!("invalid args");
    }
    let bind_to = args[1].to_socket_addrs()?.next().unwrap();
    let proxy_to = args[2].to_socket_addrs()?.next().unwrap();
    let account_server = &args[3];

    let stdout_log = tracing_subscriber::fmt::layer()
        .pretty();
    let subscriber = Registry::default()
        .with(EnvFilter::from_default_env())
        .with(stdout_log);
    tracing::subscriber::set_global_default(subscriber)
        .expect("unable to install log subscriber");

    let account_client = AccountClient::new(account_server)
        .expect("unable to construct account client");

    proxy(
        bind_to,
        proxy_to,
        Default::default(),
        Default::default(),
        account_client,
    ).await?;

    Ok(())
}
