
use std::{
    time::{
        Instant,
        Duration,
    },
    thread::sleep,
};


const NS_PER_SEC: u64 = 1_000_000_000;


pub struct TimingState {
    target_cost_per_sec: u64,
    start_time: Instant,
    cost_accum: u64,
}

impl TimingState {
    pub fn new(target_cost_per_sec: u64) -> Self {
        TimingState {
            target_cost_per_sec,
            start_time: Instant::now(),
            cost_accum: 0,
        }
    }

    pub fn reset(&mut self) {
        self.start_time = Instant::now();
        self.cost_accum = 0;
    }

    pub fn set_target_cost_per_sec(&mut self, target_cost_per_sec: u64) {
        self.target_cost_per_sec = target_cost_per_sec;
        self.reset();
    }

    pub fn throttle_sleep(&mut self, cost: u64) {
        self.cost_accum += cost;
        let target_ns_elapsed =
            self.cost_accum * NS_PER_SEC / self.target_cost_per_sec;
        let target_time =
            self.start_time + Duration::from_nanos(target_ns_elapsed);
        let now = Instant::now();

        if target_time > now {
            sleep(target_time - now)
        } else  {
            self.start_time += now - target_time;
        }
    }
}