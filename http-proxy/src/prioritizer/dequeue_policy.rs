
use super::{
    ThrottleTask,
    PrioritySub,
};
use crate::data_structures::queue_system::QueueSystem;
use repnet_util::Priority;


pub struct DequeuePolicyState {
    fifo_fraction: f32,
    cost_accum_total: u64,
    cost_accum_fifo: u64,
}

impl DequeuePolicyState {
    pub fn new(fifo_fraction: f32) -> Self {
        DequeuePolicyState {
            fifo_fraction,
            cost_accum_total: 0,
            cost_accum_fifo: 0,
        }
    }

    pub(super) fn dequeue_unwrap(
        &mut self,
        queue_system: &mut QueueSystem<u32, Priority, PrioritySub, ThrottleTask>,
    ) -> ThrottleTask
    {
        let curr_fifo = self.cost_accum_fifo as f64;
        let want_fifo =
            self.cost_accum_total as f64 * self.fifo_fraction as f64;
        
        let do_fifo = curr_fifo < want_fifo;

        let task =
            if do_fifo {
                queue_system.dequeue_fifo()
            } else {
                queue_system.dequeue_max()
            }
            .unwrap().val.val;

        self.cost_accum_total += task.cost;
        if do_fifo {
            self.cost_accum_fifo += task.cost;
        }

        task
    }

    pub fn set_fifo_fraction(&mut self, fifo_fraction: f32) {
        self.fifo_fraction = fifo_fraction;
        self.cost_accum_total = 0;
        self.cost_accum_fifo = 0;
    }
}
