//! Active system for prioritizing and throttling tasks.

mod timing;
mod dequeue_policy;


use self::{
    timing::TimingState,
    dequeue_policy::DequeuePolicyState,
};
use crate::data_structures::queue_system::{
    self,
    QueueSystem,
};
use repnet_util::Priority;
use repnet_account_client::AccountClient;
use std::{
    thread::spawn,
    sync::{
        atomic::{
            Ordering,
            AtomicBool,
        },
        Arc,
    },
};
use tokio::{
    sync::mpsc::{
        error::TryRecvError,
        UnboundedSender,
        UnboundedReceiver,
        WeakUnboundedSender,
        unbounded_channel,
    },
    task::JoinHandle,
    runtime::Handle,
};
use futures::stream::StreamExt;
use pin_utils::pin_mut;


/// Prioritizer tuning parameters.
#[derive(Debug, Clone)]
pub struct TuningParams {
    /// How many cost units to attempt to tell to run per second.
    ///
    /// If too low, can lead to resource underutilization, but if too high,
    /// can lead to suboptimal prioritization. One may set this at their
    /// estimated internet speed, or one may set it lower if they wish to avoid
    /// it monopolizing their entire network connection. Further work could
    /// attempt to modulate it dynamically by taking inspiration from
    /// congestion control algorithms. The reason we have to do this ugly thing
    /// is because the POSIX-like TCP abstraction doesn't give us enough
    /// information--it's possible we could avoid this altogether for QUIC.
    pub target_cost_per_sec: u64,
    /// Fraction of cost units to prioritize tasks on with FIFO policy rather
    /// than priority-maximization.
    ///
    /// The non-obvious reason to do this is as a sort of open-ended
    /// socialization to form mutualistic relationships which may be better
    /// than your current ones, rather than neglecting everyone else to focus
    /// solely on the peers you already know.
    pub fifo_fraction: f32,
}

impl Default for TuningParams {
    fn default() -> Self {
        TuningParams {
            // assuming the cost unit is bytes, we default initialize to
            // target 100 mebi bits per second
            target_cost_per_sec: 1 * (1 << 20) / 8,
            fifo_fraction: 0.2,
        }
    }
}


#[derive(Clone)]
pub struct Prioritizer(UnboundedSender<Req>);

impl Prioritizer {
    pub fn new(
        tuning: TuningParams,
        account_client: AccountClient,
    ) -> Self {
        assert_ne!(tuning.target_cost_per_sec, 0);
        assert!(tuning.fifo_fraction >= 0.0);
        assert!(tuning.fifo_fraction <= 1.0);

        let (send_req, recv_req) = unbounded_channel();
        
        // we spawn a hard OS thread mostly to get more granular sleeping but
        // it also may help more agressively preempt lower priority tasks
        let rt = Handle::current();
        let weak_send_req = send_req.downgrade();
        spawn(move || prioritizer_thread(
            tuning,
            recv_req,
            weak_send_req,
            rt,
            account_client,
        ));

        Prioritizer(send_req)
    }

    /// Submit a task with the given cost and optional account to the
    /// prioritizer and wait for it to tell it to run.
    ///
    /// Returns a `PrioritizerHotAccount` which can more efficiently do further
    /// throttle operations on the same account.
    pub async fn throttle(
        &self,
        key: Option<u32>,
        cost: u64,
    ) -> PrioritizerHotAccount {
        let on_account = key.is_some();
        let (send_res, mut recv_res) = unbounded_channel();
        let req = Req::EnqueueByKey {
            key,
            cost,
            send_res: send_res.clone(),
        };
        self.0.send(req).ok().expect("unexpected disconnect");
        let res = recv_res.recv().await.expect("unexpected disconnect");
        PrioritizerHotAccount {
            hai: if on_account { Some(res) } else { None },
            send_req: self.0.clone(),
            send_res,
            recv_res,
        }
    }

    #[allow(dead_code)] // TODO
    pub fn set_target_cost_per_sec(&self, target_cost_per_sec: u64) {
        let req = Req::SetTargetCostPerSec { target_cost_per_sec };
        self.0.send(req).ok().expect("unexpected disconnect");
    }

    #[allow(dead_code)] // TODO
    pub fn set_fifo_fraction(&self, fifo_fraction: f32) {
        let req = Req::SetFifoFraction { fifo_fraction };
        self.0.send(req).ok().expect("unexpected disconnect");
    }
}


#[derive(Debug)]
pub struct PrioritizerHotAccount {
    hai: Option<usize>,
    send_req: UnboundedSender<Req>,
    send_res: UnboundedSender<usize>,
    recv_res: UnboundedReceiver<usize>,
}

impl PrioritizerHotAccount {
    pub async fn throttle(&mut self, cost: u64) {
        let req = Req::EnqueueByHai {
            hai: self.hai,
            cost,
            send_res: self.send_res.clone(),
        };
        self.send_req.send(req).ok().expect("unexpected disconnect");
        self.recv_res.recv().await.expect("unexpected disconnect");
    }
}

impl Clone for PrioritizerHotAccount {
    fn clone(&self) -> Self {
        // the important part is just that a given self's send_res and recv_res
        // are linked together with each other, so this is fine.
        let (send_res, recv_res) = unbounded_channel();
        PrioritizerHotAccount {
            hai: self.hai,
            send_req: self.send_req.clone(),
            send_res,
            recv_res,
        }
    }
}

impl Drop for PrioritizerHotAccount {
    fn drop(&mut self) {
        if let Some(hai) = self.hai {
            let req = Req::DecrKeepHot { hai };
            self.send_req.send(req).ok().expect("unexpected disconnect");
        }
    }
}

fn prioritizer_thread(
    tuning: TuningParams,
    mut recv_req: UnboundedReceiver<Req>,
    weak_send_req: WeakUnboundedSender<Req>,
    rt: Handle,
    account_client: AccountClient,
) {
    let mut queue_system = QueueSystem::new();
    let mut timing = TimingState::new(tuning.target_cost_per_sec);
    let mut dequeue_policy = DequeuePolicyState::new(tuning.fifo_fraction);

    loop {
        // if queue system is empty, block on requests until it's not
        while queue_system.is_empty() {
            match recv_req.blocking_recv() {
                Some(req) => process_req(
                    req,
                    &weak_send_req,
                    &rt,
                    &account_client,
                    &mut queue_system,
                    &mut timing,
                    &mut dequeue_policy,
                ),
                None => return,
            }

            timing.reset();
        }

        // process additional requests if already present
        'poll: loop {
            match recv_req.try_recv() {
                Ok(req) => process_req(
                    req,
                    &weak_send_req,
                    &rt,
                    &account_client,
                    &mut queue_system,
                    &mut timing,
                    &mut dequeue_policy,
                ),
                Err(TryRecvError::Empty) => break 'poll,
                Err(TryRecvError::Disconnected) => return,
            }
        }
        
        // pick task according to dequeue policy
        let task = dequeue_policy.dequeue_unwrap(&mut queue_system);

        // send the response
        let _ = task.send_res.send(task.res);

        // sleep in accordance with throttling policy
        timing.throttle_sleep(task.cost);
    }
}

// item in the queue system. the prioritizer routine goes in a loop of:
// - dequeue task by prioritization policy
// - send task.to_send to task.sender
// - wait proportionally to cost
struct ThrottleTask {
    cost: u64,
    res: usize,
    send_res: UnboundedSender<usize>,
}

// requests sent to the prioritizer.
enum Req {
    // if key is some, use incr_keep_hot to convert key -> hai, and enqueue
    // throttle task which sends back hai. if key is none, enqueue throttle
    // task not on account which sends back unspecified response.
    EnqueueByKey {
        key: Option<u32>,
        cost: u64,
        send_res: UnboundedSender<usize>,
    },
    // with given hai, enqueue throttle task which sends back unspecified
    // response.
    EnqueueByHai {
        hai: Option<usize>,
        cost: u64,
        send_res: UnboundedSender<usize>,
    },
    // call decr_keep_hot
    DecrKeepHot {
        hai: usize,
    },
    // received a hot account priority change from the account server. this
    // runs into a asynchrony complication wherein a priority-watching
    // websocket stream task sends a priority update, but by the time it is
    // processed by the prioritizer thread, the account has gone cold and the
    // hot account index has been recycled. we solve that with hai_alive.
    PriorityUpdate {
        hai: usize,
        priority: Priority,
        hai_alive: Arc<AtomicBool>,
    },
    SetTargetCostPerSec {
        target_cost_per_sec: u64,
    },
    SetFifoFraction {
        fifo_fraction: f32,
    },
}

// priority subscription
struct PrioritySub {
    task: JoinHandle<()>,
    hai_alive: Arc<AtomicBool>,
}

impl Drop for PrioritySub {
    fn drop(&mut self) {
        self.task.abort();
        self.hai_alive.store(false, Ordering::Relaxed);
    }
}

fn process_req(
    req: Req,
    weak_send_req: &WeakUnboundedSender<Req>,
    rt: &Handle,
    account_client: &AccountClient,
    queue_system: &mut QueueSystem<u32, Priority, PrioritySub, ThrottleTask>,
    timing: &mut TimingState,
    dequeue_policy: &mut DequeuePolicyState,
) {
    match req {
        Req::EnqueueByKey { key, cost, send_res } => {
            let hai = key
                .map(|key| queue_system
                    .incr_keep_hot(
                        key,
                        |_, hai| {
                            let account_client = account_client.clone();
                            let weak_send_req = weak_send_req.clone();
                            let hai_alive_1 = Arc::new(AtomicBool::new(true));
                            let hai_alive_2 = Arc::clone(&hai_alive_1);
                            let task = rt
                                .spawn(async move {
                                    let stream = account_client
                                        .watch_peer_priority_retrying(key);
                                    pin_mut!(stream);
                                    loop {
                                        let priority = stream
                                            .next().await
                                            .expect("unreachable");
                                        let _ = weak_send_req
                                            .upgrade()
                                            .and_then(|send_req| send_req
                                                .send(Req::PriorityUpdate {
                                                    hai,
                                                    priority,
                                                    hai_alive: Arc::clone(&hai_alive_1),
                                                })
                                                .ok());
                                    }
                                });
                            (
                                Priority::ZERO,
                                PrioritySub {
                                    task,
                                    hai_alive: hai_alive_2,
                                },
                            )
                        }),
                    );
            let item =
                queue_system_item(ThrottleTask {
                    cost,
                    res: hai.unwrap_or(0xDEADBEEF),
                    send_res,
                });
            queue_system.enqueue(hai, item);
        }
        Req::EnqueueByHai { hai, cost, send_res } => {
            let item =
                queue_system_item(ThrottleTask {
                    cost,
                    res: 0xDEADBEEF,
                    send_res,
                });
            queue_system.enqueue(hai, item);
        }
        Req::DecrKeepHot { hai } => {
            queue_system.decr_keep_hot(hai);
        }
        Req::PriorityUpdate {
            hai,
            priority,
            hai_alive,
        } => {
            if hai_alive.load(Ordering::Relaxed) {
                queue_system.set_priority(hai, priority);
            }
        }
        Req::SetTargetCostPerSec { target_cost_per_sec } => {
            timing.set_target_cost_per_sec(target_cost_per_sec);
        }
        Req::SetFifoFraction { fifo_fraction } => {
            dequeue_policy.set_fifo_fraction(fifo_fraction);
        }
    };
}

// TODO: make API less annoying
fn queue_system_item(task: ThrottleTask) -> queue_system::Item<ThrottleTask> {
    use crate::data_structures::{
        queue_system::ItemEntry,
        links::CoercibleNode,
    };
    CoercibleNode::new(ItemEntry::new(task))
}
