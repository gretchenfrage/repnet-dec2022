
use crate::{
    prioritizer::{
        TuningParams,
        Prioritizer,
        PrioritizerHotAccount
    },
    receiver_body::ReceiverBody,
};
use repnet_account_client::AccountClient;
use std::{
    sync::Arc,
    net::SocketAddr,
    convert::Infallible,
};
use anyhow::Result;
use tokio::{
    sync::{
        mpsc::channel,
        Mutex,
    },
    net::{
        TcpListener,
        TcpStream,
    },
    task::spawn,
};
use hyper::{
    service::service_fn,
    server::conn::http1::Builder as Http1ServerConnBuilder,
    client::conn::http1::{
        Builder as Http1ClientConnBuilder,
        SendRequest as Http1SendRequest,
    },
    body::{
        Incoming as IncomingBody,
        Bytes,
    },
    header::HeaderValue,
    Request,
    Response,
};
use http_body_util::BodyExt;


pub async fn proxy(
    bind_to: SocketAddr,
    proxy_to: SocketAddr,
    ingress_tuning: TuningParams,
    egress_tuning: TuningParams,
    account_client: AccountClient,
) -> Result<Infallible>
{
    let ingress_prioritizer =
        Prioritizer::new(ingress_tuning, account_client.clone());
    let egress_prioritizer =
        Prioritizer::new(egress_tuning, account_client.clone());

    let tcp_listener = TcpListener::bind(bind_to).await?;
    loop {
        let (outer_stream, _) = tcp_listener.accept().await?;

        let ingress_prioritizer = ingress_prioritizer.clone();
        let egress_prioritizer = egress_prioritizer.clone();
        let account_client = account_client.clone();

        spawn(async move {
            let result =
                handle_connection(
                    proxy_to,
                    outer_stream,
                    ingress_prioritizer,
                    egress_prioritizer,
                    account_client,
                ).await;
            if let Err(e) = result {
                error!(%e, "error serving connection");
            }
        });
    }
}

async fn handle_connection(
    proxy_to: SocketAddr,
    outer_stream: TcpStream,
    ingress_prioritizer: Prioritizer,
    egress_prioritizer: Prioritizer,
    account_client: AccountClient,
) -> Result<()> {
    let inner_client_slot = Arc::new(Mutex::new(None));
    Http1ServerConnBuilder::new()
        .serve_connection(
            outer_stream,
            service_fn(move |outer_req| serve_request(
                proxy_to,
                inner_client_slot.clone(),
                outer_req,
                ingress_prioritizer.clone(),
                egress_prioritizer.clone(),
                account_client.clone()
            )),
        ).await?;
    Ok(())
}

type InnerClient = Http1SendRequest<ReceiverBody<Bytes>>;

async fn serve_request(
    proxy_to: SocketAddr,
    inner_client_slot: Arc<Mutex<Option<InnerClient>>>,
    outer_req: Request<IncomingBody>,
    ingress_prioritizer: Prioritizer,
    egress_prioritizer: Prioritizer,
    account_client: AccountClient,
) -> Result<Response<ReceiverBody<Bytes>>>
{
    // read prioritization-relevant headers
    let key = outer_req.headers()
        .get("repnet-foobar")
        .and_then(|header| match parse_foobar_header(header) {
            Ok(foobar) => Some(foobar),
            Err(e) => {
                error!(%e, "malformed repnet-foobar header");
                None
            }
        });

    // ingress throttle
    let ingress_phc = ingress_prioritizer.throttle(key, 0).await;

    // prepare inner request with ingress body relay
    let (req_parts, outer_req_body) = outer_req.into_parts();
    let ingress_billing = key
        .map(|key| Billing {
            account_client: account_client.clone(),
            peer: key,
            byte_unit: "inet-ingress-bytes",
        });
    let inner_req_body =
        spawn_body_relay(
            outer_req_body,
            ingress_phc,
            ingress_billing,
        );
    let inner_req = Request::from_parts(req_parts, inner_req_body);

    // relay inner request, initializing new HTTP stream if appropriate
    // and wait for inner response
    let inner_res = {
        let mut inner_client_slot = inner_client_slot.lock().await;
        if inner_client_slot.is_none() {
            let inner_stream = TcpStream::connect(proxy_to).await?;
            let (
                inner_client,
                inner_conn,
            ) = Http1ClientConnBuilder::new().handshake(inner_stream).await?;
            spawn(inner_conn);
            *inner_client_slot = Some(inner_client);
        }
        let inner_client = inner_client_slot.as_mut().unwrap();
        inner_client.send_request(inner_req).await?
    };

    // egress throttle
    let egress_phc = egress_prioritizer.throttle(key, 0).await;

    // prepare outer response with egress body relay
    let (res_parts, inner_res_body) = inner_res.into_parts();
    let eggress_billing = key
        .map(|key| Billing {
            account_client: account_client.clone(),
            peer: key,
            byte_unit: "inet-egress-bytes",
        });
    let outer_res_body =
        spawn_body_relay(
            inner_res_body,
            egress_phc,
            eggress_billing,
        );
    let outer_res = Response::from_parts(res_parts, outer_res_body);

    // return it
    Ok(outer_res)
}

#[derive(Clone)]
struct Billing {
    account_client: AccountClient,
    peer: u32,
    byte_unit: &'static str,
}

impl Billing {
    fn bill(&self, num_bytes: u64) {
        let billing = self.clone();
        spawn(async move {
            billing
                .account_client
                .bill_peer_retrying(
                    billing.peer,
                    billing.byte_unit,
                    num_bytes as f64,
                ).await;
        });
    }
}

fn spawn_body_relay(
    mut body: IncomingBody,
    mut phc: PrioritizerHotAccount,
    billing: Option<Billing>,
) -> ReceiverBody<Bytes>
{
    const BODY_RELAY_CHANNEL_SIZE: usize = 64;
    let (send_frame, recv_frame) = channel(BODY_RELAY_CHANNEL_SIZE);
    spawn(async move {
        while let Some(frame) = body
            .frame().await
            .and_then(|result| match result {
                Ok(frame) => Some(frame),
                Err(e) => {
                    error!(%e, "error receiving body frame");
                    None
                }
            })
        {
            let cost = frame
                .data_ref()
                .map(|data| data.len() as u64)
                .unwrap_or(0);
            phc.throttle(cost).await;
            if cost > 0 {
                if let Some(ref billing) = billing {
                    billing.bill(cost);
                }
            }
            let send_result = send_frame.send(frame).await;
            if send_result.is_err() {
                return;
            }
        }
    });
    ReceiverBody(recv_frame)
}

fn parse_foobar_header(header: &HeaderValue) -> Result<u32> {
    Ok(header.to_str()?.parse()?)
}
