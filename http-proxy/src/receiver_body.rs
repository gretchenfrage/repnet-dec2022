
use std::{
    pin::Pin,
    task::{
        Context,
        Poll,
    },
    convert::Infallible,
};
use tokio::sync::mpsc::Receiver;
use hyper::body::{
    Body,
    Buf,
    Frame,
};


/// Adapter to create hyper `Body` from `tokio::sync::mpsc::Receiver` of
/// frames.
pub struct ReceiverBody<T>(pub Receiver<Frame<T>>);

impl<T: Buf> Body for ReceiverBody<T> {
    type Data = T;
    type Error = Infallible;

    fn poll_frame(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
    ) -> Poll<Option<Result<Frame<Self::Data>, Self::Error>>> {
        self.0.poll_recv(cx)
            .map(|option| option
                .map(|frame| Ok(frame)))
    }
}
