//! Intrusive linked lattice queue system.

use std::{
    ptr::NonNull,
    fmt::{self, Formatter, Debug},
};


pub const NUM_LANES: usize = 2;

pub struct Queue<T: ?Sized, const LANE: usize> {
    back: Option<NonNull<Node<T,>>>,
    front: Option<NonNull<Node<T>>>,
}

// TODO: could optimize memory more by doing some clever stuff with fat
//       pointers
pub struct CoercibleNode<T1: ?Sized, T2: ?Sized> {
    lanes_prev: [Option<NonNull<Node<T1>>>; NUM_LANES],
    lanes_next: [Option<NonNull<Node<T1>>>; NUM_LANES],
    pub val: T2,
}

unsafe impl<T1: ?Sized + Send, T2: ?Sized + Send> Send for CoercibleNode<T1, T2> {}

pub type Node<T> = CoercibleNode<T, T>;

impl<T: ?Sized, const LANE: usize> Queue<T, LANE> {
    pub fn new() -> Self {
        assert!(LANE < NUM_LANES, "lane out of bounds");
        Queue { back: None, front: None }
    }

    pub fn is_empty(&self) -> bool {
        self.front.is_none()
    }

    pub fn peek(&self) -> Option<&T> {
        unsafe {
            self.front.map(|front| &front.as_ref().val)
        }
    }
}

impl<T1: ?Sized, T2> CoercibleNode<T1, T2> {
    pub fn new(val: T2) -> Box<Self> {
        Box::new(CoercibleNode {
            lanes_prev: [None; NUM_LANES],
            lanes_next: [None; NUM_LANES],
            val,
        })
    }
}

// like a for loop, but works nice with enumerating lanes
macro_rules! for_lanes {
    (
        for ($i:pat, $v:pat) in [$lane_0:expr, $lane_1:expr] { $($t:tt)* }
    )=>{{
        { let $i = 0; let $v = $lane_0; $($t)* }
        { let $i = 1; let $v = $lane_1; $($t)* }
    }};
}

impl<T: ?Sized> Node<T> {
    pub fn push(
        self: Box<Self>,
        lane_0_queue: Option<&mut Queue<T, 0>>,
        lane_1_queue: Option<&mut Queue<T, 1>>,
    ) {
        unsafe {
            let mut node = NonNull::new_unchecked(Box::into_raw(self));
            for_lanes!(for (lane, queue) in [lane_0_queue, lane_1_queue] {
                if let Some(queue) = queue {
                    if let Some(ref mut back) = queue.back {
                        // adding to non-empty queue
                        back.as_mut().lanes_prev[lane] = Some(node);
                        node.as_mut().lanes_next[lane] = Some(*back);
                        *back = node;
                    } else {
                        // adding to empty queue
                        queue.back = Some(node);
                        queue.front = Some(node);
                    }
                }
            });
        }
    }
}

pub fn pop<T: ?Sized>(
    target_lane: usize,
    mut lane_0_queue: Option<&mut Queue<T, 0>>,
    mut lane_1_queue: Option<&mut Queue<T, 1>>,
) -> Box<Node<T>> {
    unsafe {
        let mut node: NonNull<Node<T>> =
            match target_lane {
                0 => lane_0_queue.as_ref().expect("target queue given as None").front,
                1 => lane_1_queue.as_ref().expect("target queue given as None").front,
                _ => panic!("lane out of bounds"),
            }
            .expect("target queue is empty");

        // soundness guard
        for_lanes!(for (lane, ref queue) in [lane_0_queue, lane_1_queue] {
            if let Some(ref queue) = queue {
                if node.as_ref().lanes_prev[lane].is_none() {
                    assert!(queue.back == Some(node), "soundness guard triggered");
                }
                if node.as_ref().lanes_next[lane].is_none() {
                    assert!(queue.front == Some(node), "soundness guard triggered");
                }
            }
        });

        // remove from queues
        for_lanes!(for (lane, ref mut queue) in [lane_0_queue, lane_1_queue] {
            if let Some(ref mut queue) = queue {
                if let Some(mut prev) = node.as_mut().lanes_prev[lane] {
                    prev.as_mut().lanes_next[lane] = node.as_ref().lanes_next[lane];
                } else {
                    queue.back = node.as_ref().lanes_next[lane];
                }
                if let Some(mut next) = node.as_mut().lanes_next[lane] {
                    next.as_mut().lanes_prev[lane] = node.as_ref().lanes_prev[lane];
                } else {
                    queue.front = node.as_ref().lanes_prev[lane];
                }
            }
        });

        // reset node's pointers
        node.as_mut().lanes_prev = [None; NUM_LANES];
        node.as_mut().lanes_next = [None; NUM_LANES];

        // done
        Box::from_raw(node.as_ptr())
    }
}

impl<T: ?Sized + Debug, const LANE: usize> Debug for Queue<T, LANE> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mut list = f.debug_list();
        unsafe {
            let mut next = self.back;
            while let Some(curr) = next {
                next = curr.as_ref().lanes_next[LANE];
                list.entry(&&curr.as_ref().val);
            }
        }
        list.finish()
        /*
        struct D<'a, T: ?Sized>(&'a Node<T>);
        impl<'a, T: ?Sized + Debug> Debug for D<'a, T> {
            fn fmt(&self, f: &mut Formatter) -> fmt::Result {
                f.debug_struct("")
                    .field("address", &(self.0 as *const _))
                    .field("lanes_prev", &self.0.lanes_prev)
                    .field("lanes_next", &self.0.lanes_next)
                    .field("val", &&self.0.val)
                    .finish()
            }
        }

        let mut list = f.debug_list();
        unsafe {
            let mut next = self.back;
            while let Some(curr) = next {
                next = curr.as_ref().lanes_next[LANE];
                list.entry(&D(curr.as_ref()));
            }
        }
        list.finish()
        */
    }
}

impl<T1: ?Sized, T2: ?Sized + Debug> Debug for CoercibleNode<T1, T2> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        Debug::fmt(&&self.val, f)
        /*
        f.debug_struct("Node")
            .field("address", &(self as *const _))
            .field("val", &&self.val)
            .finish()
        */
    }
}

#[test]
fn test_unsizing_type_magic() {
    #[allow(dead_code)]
    struct Foo<T: ?Sized> {
        a: i32,
        b: T,
    }

    let node: Box<CoercibleNode<Foo<[u8]>, Foo<[u8; 3]>>> = CoercibleNode::new(Foo { a: 1, b: [2, 3, 4] });
    let _node: Box<Node<Foo<[u8]>>> = node as _;
}

#[test]
fn test_queue_lattice() {
    for i in 1..=5 {
        let mut queue_0_foo: Queue<char, 0> = Queue::new();
        let mut queue_0_bar: Queue<char, 0> = Queue::new();
        let mut queue_1: Queue<char, 1> = Queue::new();

        let node_a = CoercibleNode::new('a');
        let node_b = CoercibleNode::new('b');
        let node_c = CoercibleNode::new('c');

        node_a.push(Some(&mut queue_0_foo), Some(&mut queue_1));
        node_b.push(Some(&mut queue_0_bar), Some(&mut queue_1));
        node_c.push(Some(&mut queue_0_foo), Some(&mut queue_1));

        match i {
            1 => {            
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'a',
                );
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_bar),
                        Some(&mut queue_1),
                    ).val,
                    'b',
                );
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'c',
                );
            }
            2 => {
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'a',
                );
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_bar),
                        Some(&mut queue_1),
                    ).val,
                    'b',
                );
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'c',
                );
            }
            3 => {
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_bar),
                        Some(&mut queue_1),
                    ).val,
                    'b',
                );
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'a',
                );
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'c',
                );
            }
            4 => {
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'a',
                );
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_bar),
                        Some(&mut queue_1),
                    ).val,
                    'b',
                );
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'c',
                );
            }
            5 => {
                assert_eq!(
                    pop(
                        0,
                        Some(&mut queue_0_bar),
                        Some(&mut queue_1),
                    ).val,
                    'b',
                );
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'a',
                );
                assert_eq!(
                    pop(
                        1,
                        Some(&mut queue_0_foo),
                        Some(&mut queue_1),
                    ).val,
                    'c',
                );
            }
            _ => unreachable!()
        }
    }
}
