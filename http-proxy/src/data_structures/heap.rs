//! Max-heap with resifting support.

use std::ops::{
    Deref,
    DerefMut,
};


/// Max-heap which supports _resifting_, allowing one to change an entry's
/// key and then O(log(N)) update the heap to restore invariants.
///
/// Exposes the underlying array through deref, similarly to `Vec`. Index 0 is
/// the max entry. Any modification to an entry's key should be immediately
/// followed by a matching call to `resift`.
#[derive(Debug, Clone)]
pub struct Heap<K, V>(Vec<Entry<K, V>>);

/// Element of `Heap`.
#[derive(Debug, Clone)]
pub struct Entry<K, V> {
    pub key: K,
    pub val: V,
}

impl<K, V> Deref for Heap<K, V> {
    type Target = [Entry<K, V>];

    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

impl<K, V> DerefMut for Heap<K, V> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut *self.0
    }
}

impl<K, V> Heap<K, V> {
    pub fn new() -> Self {
        Heap(Vec::new())
    }

    fn child_1(&self, i: usize) -> Option<usize> {
        Some(nonchecking_child_1(i)).filter(|&j| j < self.len())
    }

    fn child_2(&self, i: usize) -> Option<usize> {
        Some(nonchecking_child_2(i)).filter(|&j| j < self.len())
    }
}

impl<K: Ord, V> Heap<K, V> {
    /// Insert entry. Returns final index of inserted entry. See `resift`
    /// regarding `on_relocate`.
    pub fn insert<F>(&mut self, key: K, val: V, on_relocate: F) -> usize
    where
        F: FnMut(&Entry<K, V>, usize, usize),
    {
        let entry = Entry { key, val };

        let idx = self.len();
        self.0.push(entry);
        self.resift(idx, on_relocate)
    }

    /// Remove and return entry at given index. See `resift` regarding
    /// `on_relocate`.
    ///
    /// Panics if index out of bounds.
    pub fn remove<F>(&mut self, idx: usize, on_relocate: F) -> Entry<K, V>
    where
        F: FnMut(&Entry<K, V>, usize, usize),
    {
        let entry = self.0.swap_remove(idx);
        if idx != self.len() {
            self.resift(idx, on_relocate);
        }
        entry
    }

    /// Resift entry at given index, causing some number of swaps (maybe 0)
    /// with parent xor a child.
    ///
    /// `on_relocate` is called for every _other_ entry (not the targeted one)
    /// that's moved, with (refernce to the other entry, its old index, its
    /// new index).
    ///
    /// Returns the final resifted index of the _target_ entry.
    ///
    /// Panics if index out of bounds.
    pub fn resift<F>(&mut self, idx: usize, mut on_relocate: F) -> usize
    where
        F: FnMut(&Entry<K, V>, usize, usize),
    {
        assert!(idx < self.len(), "index out of bounds");
        
        let mut i = idx;
        while let Some(j) = self.should_swap_with(i) {
            on_relocate(&self[j], j, i);
            self.swap(i, j);
            i = j;
        }
        i
    }

    fn should_swap_with(&self, i: usize) -> Option<usize> {
        macro_rules! first_some {
            ($a:expr,)=>{ $a };
            ($a:expr, $($b:tt)*)=>{ $a.or_else(|| first_some!($($b)*)) };
        }
        first_some!(
            parent(i).filter(|&j| self[i].key > self[j].key),
            self.child_1(i).filter(|&j| self[i].key < self[j].key),
            self.child_2(i).filter(|&j| self[i].key < self[j].key),
        )
    }
}

fn parent(i: usize) -> Option<usize> { i.checked_sub(1).map(|n| n / 2) }

fn nonchecking_child_1(i: usize) -> usize { 2 * i + 1 }

fn nonchecking_child_2(i: usize) -> usize { 2 * i + 2 }
