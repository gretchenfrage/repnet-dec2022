//! Priority-queueing system of data structures.

use crate::data_structures::{
    heap::{self, Heap},
    links::{
        Queue,
        Node,
        pop,
    },
};
use repnet_util::Priority;
use std::{
    collections::HashMap,
    hash::Hash,
};
use slab::Slab;


/// Extension to `Ord` to check whether a value is not greater than any other
/// values in its type.
pub trait IsMin {
    fn is_min(&self) -> bool;
}

macro_rules! uint_is_min {
    ($($i:ty),*)=>{ $(
        impl IsMin for $i { fn is_min(&self) -> bool { *self == 0 } }
    )* };
}

uint_is_min!(u8, u16, u32, u64, u128);

impl IsMin for Priority {
    fn is_min(&self) -> bool {
        *self == Priority::ZERO
    }
}


const FIFO_LANE: usize = 0;
const ACCOUNT_LANE: usize = 1;


// TODO: could make Clone for debugging purposes, but would be tricky because
//       of the queue lattice being all complicated with raw pointers and stuff

/// Task priority-queueing data structure system for repnet's needs.
///
/// Type-parameters:
///
/// - `K`: key, identifies accounts
/// - `O`: order-giving property, accounts prioritized by
/// - `A`: additional hot account data
/// - `T`: item type, put in an account's queue
#[derive(Debug)]
pub struct QueueSystem<K, O, A, T: ?Sized> {
    fifo: Queue<ItemEntry<T>, FIFO_LANE>,
    // key -> slab index
    // which we externally call "hot account index"
    // TODO: could avoid cloning K by using hashbrown raw hashmaps
    hmap: HashMap<K, usize>,
    slab: Slab<SlabEntry<K, O, A>>,
    heap: Heap<O, HeapEntry<T>>,
}

// Queue is not Send for similar reasons to why Rc isn't send
// but, similarly
// so long as we don't allow the user to take or give ownership of Queues
// with the QueueSystem, we force the entire connected lattice of queues
// to all move together when it moves between threads, so it's fine
unsafe impl<
    K: Send,
    O: Send,
    A: Send,
    T: ?Sized + Send,
> Send for QueueSystem<K, O, A, T> {}

// slab entries exist for all hot accounts, that is, all accounts loaded into
// the queue system
#[derive(Debug)]
struct SlabEntry<K, O, A> {
    keep_hot_count: u64,
    key: K,
    // TODO: could avoid cloning O by making heap raw hashmap-like
    ord: O,
    // none iff this account's queue is empty
    heap_idx: Option<usize>,
    #[allow(dead_code)] // for now we solely rely on it to do stuff via its
                        // destructor I guess
    additional: A,
}

// slab entries exist for all accounts with non-empty queues, that being a
// subset of hot accounts
#[derive(Debug)]
struct HeapEntry<T: ?Sized> {
    slab_idx: usize,
    // must be kept nonempty. must remove heap entry if becomes empty.
    queue: Queue<ItemEntry<T>, ACCOUNT_LANE>,
}

// item entries exist for all enqueued items, within the lattice queues
#[derive(Debug)]
pub struct ItemEntry<T: ?Sized> {
    // none iff this item is not associated with an account
    // TODO: we could optimize memory by using high bits as discriminant
    slab_idx: Option<usize>,
    pub val: T,
}

/// Fat-compatible heap allocation of item and additional fields for queue
/// management.
pub type Item<T> = Box<Node<ItemEntry<T>>>;

impl<K, O, A, T: ?Sized> QueueSystem<K, O, A, T> {
    pub fn new() -> Self {
        QueueSystem {
            fifo: Queue::new(),
            hmap: HashMap::new(),
            slab: Slab::new(),
            heap: Heap::new(),
        }
    }
}

impl<K: Clone + Eq + Hash, O: Clone + Ord + IsMin, A, T: ?Sized> QueueSystem<K, O, A, T> {
    /// Increment the keep-hot count of the account specified by key,
    /// allocating a new hot account entry if necessary, and return its hot
    /// account index.
    pub fn incr_keep_hot<F>(&mut self, key: K, create_data: F) -> usize
    where
        F: FnOnce(&K, usize) -> (O, A),
    {
        let hai = *self.hmap.entry(key)
            .or_insert_with_key(|key| {
                let entry = self.slab.vacant_entry();
                let hai = entry.key();
                let (ord, additional) = create_data(key, hai);
                entry.insert(SlabEntry {
                    keep_hot_count: 1,
                    key: key.clone(),
                    ord,
                    heap_idx: None,
                    additional,
                });
                hai
            });
        self.slab[hai].keep_hot_count += 1;
        hai
    }

    /// Decrement the keep-hot count of the account specified by hot account
    /// index, possibly deallocating the hot account entry and invalidating the
    /// hot account index.
    ///
    /// Assumes hot account index is valid, and keep-hot count is positive, or
    /// logic errors may occur.
    pub fn decr_keep_hot(&mut self, hai: usize) {
        let slab_entry = &mut self.slab[hai];
        slab_entry.keep_hot_count -= 1;
        if slab_entry.keep_hot_count == 0 && slab_entry.heap_idx.is_none() {
            self.hmap.remove(&slab_entry.key);
            self.slab.remove(hai);
        }
    }
    
    pub fn enqueue(
        &mut self,
        hai: Option<usize>,
        item: Item<T>,
    ) {
        match hai {
            Some(hai) => self.enqueue_some_account(hai, item),
            None => self.enqueue_none_account(item),
        }
    }
    
    /// Enqueue an item associated with the given account specified by hot
    /// account index.
    ///
    /// Assumes hot account index is valid, or logic errors may occur.
    pub fn enqueue_some_account(&mut self, hai: usize, mut item: Item<T>) {
        let slab_idx = hai;

        item.val.slab_idx = Some(slab_idx);
        if let Some(heap_idx) = self.slab[slab_idx].heap_idx {
            item.push(
                    Some(&mut self.fifo),
                    Some(&mut self.heap[heap_idx].val.queue),
                );
        } else {
            let mut queue = Queue::new();
            item.push(
                    Some(&mut self.fifo),
                    Some(&mut queue),
                );
            let heap_idx = self.heap
                .insert(
                    self.slab[slab_idx].ord.clone(),
                    HeapEntry { slab_idx, queue },
                    heap_relocate_handler(&mut self.slab),
                );
            self.slab[slab_idx].heap_idx = Some(heap_idx);
        }
    }

    /// Enqueue an item not associated with any account.
    pub fn enqueue_none_account(&mut self, item: Item<T>) {
        item.push(
                Some(&mut self.fifo),
                None,
            );
    }

    pub fn is_empty(&self) -> bool {
        self.fifo.peek().is_none()
    }

    /// Dequeue item of maximal priority if nonempty.
    pub fn dequeue_max(&mut self) -> Option<Item<T>> {
        if self.heap.is_empty() || self.heap[0].key.is_min() {
            self.dequeue_fifo()
        } else {
            let heap_entry = &mut self.heap[0].val;
            let mut item = pop(
                ACCOUNT_LANE,
                Some(&mut self.fifo),
                Some(&mut heap_entry.queue),
            );
            item.val.slab_idx = None;
            if heap_entry.queue.is_empty() {
                let slab_entry = &mut self.slab[heap_entry.slab_idx];
                if slab_entry.keep_hot_count == 0 {
                    self.hmap.remove(&slab_entry.key);
                    self.slab.remove(heap_entry.slab_idx);
                } else {
                    slab_entry.heap_idx = None;
                }
                self.heap.remove(0, heap_relocate_handler(&mut self.slab));
            }

            Some(item)
        }
    }

    /// Dequeue item in pure FIFO pattern if nonempty.
    pub fn dequeue_fifo(&mut self) -> Option<Item<T>> {
        if let Some(item_entry) = self.fifo.peek() {
            if let Some(slab_idx) = item_entry.slab_idx {
                let heap_idx = self.slab[slab_idx].heap_idx.unwrap();
                let account_queue = &mut self.heap[heap_idx].val.queue;
                let mut item = pop(
                    FIFO_LANE,
                    Some(&mut self.fifo),
                    Some(account_queue),
                );
                item.val.slab_idx = None;
                if account_queue.is_empty() {
                    if self.slab[slab_idx].keep_hot_count == 0 {
                        self.hmap.remove(&self.slab[slab_idx].key);
                        self.slab.remove(slab_idx);
                    } else {
                        self.slab[slab_idx].heap_idx = None;
                    }
                    self.heap.remove(heap_idx, heap_relocate_handler(&mut self.slab));
                }
                Some(item)
            } else {
                Some(pop(
                    FIFO_LANE,
                    Some(&mut self.fifo),
                    None,
                ))
            }
        } else {
            None
        }
    }

    /// Update the priority of the account specified by hot account index.
    ///
    /// Assumes hot account index is valid, or logic errors may occur.
    pub fn set_priority(&mut self, hai: usize, ord: O) {
        let slab_entry = &mut self.slab[hai];
        slab_entry.ord = ord;
        if let Some(heap_idx) = slab_entry.heap_idx {
            self.heap[heap_idx].key = slab_entry.ord.clone();
            self.heap.resift(heap_idx, heap_relocate_handler(&mut self.slab));
        }
    }
}

fn heap_relocate_handler<'a, K, O, A, T: ?Sized>(
    slab: &'a mut Slab<SlabEntry<K, O, A>>,
) -> impl FnMut(&heap::Entry<O, HeapEntry<T>>, usize, usize) + 'a {
    |heap_entry, _, heap_entry_new_idx| {
        slab[heap_entry.val.slab_idx].heap_idx = Some(heap_entry_new_idx);
    }
}

impl<T> ItemEntry<T> {
    pub fn new(val: T) -> Self {
        ItemEntry {
            slab_idx: None,
            val,
        }
    }
}


#[test]
fn queue_system_test() {
    fn item(n: i32) -> Item<i32> { Node::new(ItemEntry::new(n)) }

    let mut system: QueueSystem<i32, u32, (), i32> = QueueSystem::new();

    system.enqueue_no_account(item(1));
    system.enqueue_no_account(item(2));
    system.enqueue_no_account(item(3));

    assert_eq!(system.dequeue_fifo().unwrap().val.val, 1);
    assert_eq!(system.dequeue_fifo().unwrap().val.val, 2);
    assert_eq!(system.dequeue_fifo().unwrap().val.val, 3);

    let mut system: QueueSystem<i32, u32, (), i32> = QueueSystem::new();

    system.enqueue_no_account(item(1));
    system.enqueue_no_account(item(2));
    system.enqueue_no_account(item(3));

    assert_eq!(system.dequeue_max().unwrap().val.val, 1);
    assert_eq!(system.dequeue_max().unwrap().val.val, 2);
    assert_eq!(system.dequeue_max().unwrap().val.val, 3);

    let mut system: QueueSystem<i32, u32, (), i32> = QueueSystem::new();

    let hai_1 = system.incr_keep_hot(1001, |_| (70, ()));
    let hai_2 = system.incr_keep_hot(1002, |_| (50, ()));

    system.enqueue(hai_1, item(1));
    system.enqueue(hai_2, item(2));
    system.enqueue(hai_1, item(3));
    system.enqueue(hai_2, item(4));
    system.enqueue(hai_1, item(5));

    system.decr_keep_hot(hai_2);

    assert_eq!(system.dequeue_max().unwrap().val.val, 1);
    assert_eq!(system.dequeue_fifo().unwrap().val.val, 2);
    assert_eq!(system.dequeue_max().unwrap().val.val, 3);
    assert_eq!(system.dequeue_max().unwrap().val.val, 5);
    assert_eq!(system.dequeue_max().unwrap().val.val, 4);

    system.decr_keep_hot(hai_1);

    let mut system: QueueSystem<i32, u32, (), i32> = QueueSystem::new();

    let hai_1 = system.incr_keep_hot(1001, |_| (70, ()));
    let hai_2 = system.incr_keep_hot(1002, |_| (50, ()));

    system.enqueue(hai_1, item(1));
    system.enqueue(hai_1, item(2));
    system.enqueue(hai_1, item(3));
    system.enqueue(hai_1, item(4));

    system.enqueue(hai_2, item(5));
    system.enqueue(hai_2, item(6));
    system.enqueue(hai_2, item(7));
    system.enqueue(hai_2, item(8));

    assert_eq!(system.dequeue_max().unwrap().val.val, 1);
    assert_eq!(system.dequeue_max().unwrap().val.val, 2);

    system.set_priority(hai_2, 90);

    assert_eq!(system.dequeue_max().unwrap().val.val, 5);
    assert_eq!(system.dequeue_max().unwrap().val.val, 6);
    assert_eq!(system.dequeue_max().unwrap().val.val, 7);
    assert_eq!(system.dequeue_max().unwrap().val.val, 8);

    assert_eq!(system.dequeue_max().unwrap().val.val, 3);
    assert_eq!(system.dequeue_max().unwrap().val.val, 4);

    system.decr_keep_hot(hai_1);
    system.decr_keep_hot(hai_2);
}
