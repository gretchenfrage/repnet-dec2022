
pub fn priority(tally: f64, mut total: f64, trust: f64) -> f64 {
    debug_assert!(trust >= 0.0);

    if total < 0.0 {
        warn!(%total, "total < 0.0 (bottoming out)");
        total = 0.0;
    }
    
    if tally < 0.0 {
        trace!("case 1");
        1.0
    } else if total == 0.0 {
        trace!("case 2");
        0.0
    } else {
        trace!("case 3");
        fn pow2(n: f64) -> f64 { n * n }
        let cap = total * trust;
        pow2(cap) / pow2(tally + cap)
    }
}

pub fn bill(rep: f64, tally: &mut f64, total: f64) {
    debug_assert!(rep >= 0.0);
    debug_assert!(total >= 0.0);

    if total > 0.0 {
        *tally += rep;
    } else {
        trace!(%rep, "auto-freeloading");
    }
}

pub fn credit(rep: f64, tally: &mut f64, total: &mut f64) {
    debug_assert!(rep >= 0.0);
    debug_assert!(*total >= 0.0);

    *tally -= rep;
    *total += rep;
}
