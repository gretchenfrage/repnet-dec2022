
use std::{
    ops::Deref,
    fmt::{self, Formatter, Display},
};
use anyhow::*;


pub const UNIT_INET_EGRESS_BYTES: UnitName<&'static str> =
    UnitName("inet-egress-bytes");
pub const UNIT_INET_INGRESS_BYTES: UnitName<&'static str> =
    UnitName("inet-ingress-bytes");


#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct UnitName<S>(S);

impl<S: AsRef<str>> UnitName<S> {
    pub fn new(name: S) -> Result<Self> {
        {
            let name = name.as_ref();
            ensure!(
                !name.is_empty(),
                "empty unit name",
            );
            for c in name.chars() {
                ensure!(
                    matches!(c, 'a'..='z' | 'A'..='Z' | '0'..='9' | '-'),
                    "unit name contains invalid char {:?}",
                    c,
                );
            }
        }
        Ok(UnitName(name))
    }

    pub fn as_ref(&self) -> UnitName<&str> {
        UnitName(self.0.as_ref())
    }

    pub fn to_owned(&self) -> UnitName<String> {
        UnitName(self.0.as_ref().to_owned())
    }
}

impl<S: AsRef<str>> Deref for UnitName<S> {
    type Target = str;

    fn deref(&self) -> &str {
        self.0.as_ref()
    }
}

impl<S: Display> Display for UnitName<S> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        <S as Display>::fmt(&self.0, f)
    }
}
