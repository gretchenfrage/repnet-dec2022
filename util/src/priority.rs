
use std::cmp::Ordering;
use anyhow::*;


/// Float between 0 and 1.
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub struct Priority(f64);

impl Priority {
    pub const ZERO: Priority = Priority(0.0);
    pub const ONE: Priority = Priority(1.0);

    pub fn new(priority: f64) -> Result<Self> {
        ensure!(
            priority >= 0.0,
            "priority {} not >= 0",
            priority,
        );
        ensure!(
            priority <= 1.0,
            "priority {} not <= 1",
            priority,
        );
        Ok(Priority(priority))
    }

    pub fn get(self) -> f64 {
        self.0
    }
}

impl Eq for Priority {}

impl Ord for Priority {
    fn cmp(&self, rhs: &Self) -> Ordering {
        PartialOrd::partial_cmp(self, rhs).unwrap()
    }
}
