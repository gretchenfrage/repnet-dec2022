
#[macro_use]
extern crate tracing;


use repnet_util::Priority;
use std::time::Duration;
use anyhow::*;
use url::Url;
use futures::stream::{
    Stream,
    StreamExt,
};
use tokio_tungstenite::{
    connect_async,
    tungstenite::protocol::Message,
};
use reqwest::Client;
use tokio::time::sleep;
use async_stream::stream;
use pin_utils::pin_mut;


#[derive(Debug, Clone)]
pub struct AccountClient {
    server_url: Url,
    client: Client,
}

impl AccountClient {
    pub fn new(server_url: &str) -> Result<Self>
    {
        let server_url = Url::parse(server_url)?;
        ensure!(
            !server_url.cannot_be_a_base(),
            "server url {} cannot be a base",
            server_url,
        );
        ensure!(
            matches!(server_url.scheme(), "http" | "https"),
            "server url {} must have http or https scheme",
            server_url,
        );
        let client = Client::builder().build()?;
        Ok(AccountClient {
            server_url,
            client,
        })
    }

    pub async fn get_peer_priority(&self, peer: u32) -> Result<Priority>
    {
        trace!(%peer, "AccountClient.get_peer_priority");
        let mut url = self.server_url.clone();
        url.path_segments_mut().unwrap()
            .push("peer")
            .push(peer.to_string().as_str())
            .push("priority");
        let response = self.client
            .get(url)
            .send().await?;
        ensure!(
            response.status().is_success(),
            "error {}",
            response.status(),
        );
        let priority = response.json::<f64>().await?;
        let priority = Priority::new(priority)?;
        trace!(%peer, ?priority, "AccountClient.get_peer_priority returning");
        Ok(priority)
    }

    pub async fn watch_peer_priority(
        &self,
        peer: u32,
    ) -> Result<impl Stream<Item=Result<Priority>>>
    {
        trace!(%peer, "AccountClient.watch_peer_priority");
        let mut url = self.server_url.clone();
        url.path_segments_mut().unwrap()
            .push("peer")
            .push(peer.to_string().as_str())
            .push("priority")
            .push("watch");
        let ws_scheme =
            match url.scheme() {
                "http" => "ws",
                "https" => "wss",
                _ => unreachable!(),
            };
        url.set_scheme(ws_scheme).unwrap();
        let (ws, _) = connect_async(url).await?;
        let stream = ws
            .filter_map(|result| async move {
                result
                    .map_err(Error::from)
                    .and_then(|msg| match msg {
                        Message::Text(text) => Ok(Some(text)),
                        Message::Binary(_) => Err(anyhow!("binary message")),
                        _ => Ok(None),
                    })
                    .transpose()
                    .map(|result| result
                        .and_then(|text| serde_json::from_str::<f64>(&text)
                            .map_err(Error::from))
                        .and_then(Priority::new)
                        .map(|priority| {
                            trace!(?priority, "AccountClient.watch_peer_priority item");
                            priority
                        }))
            });
        Ok(stream)
    }

    /// `watch_peer_priority` except it handles any sort of failure by logging
    /// the error and retrying after 1 second.
    pub fn watch_peer_priority_retrying(
        &self,
        peer: u32,
    ) -> impl Stream<Item=Priority> {
        trace!(%peer, "AccountClient.watch_peer_priority_retrying");
        let client = self.clone();
        stream! {
            loop {
                match client.watch_peer_priority(peer).await {
                    Result::Ok(stream) => {
                        pin_mut!(stream);
                        'inner: loop {
                            match stream.next().await {
                                Some(Result::Ok(priority)) => {
                                    yield priority;
                                }
                                Some(Result::Err(e)) => {
                                    error!(%e, "watch peer priority error");
                                    break 'inner
                                }
                                None => {
                                    error!("watch peer priority stream closed");
                                    break 'inner
                                }
                            }
                        }
                    }
                    Result::Err(e) => {
                        error!(%e, "watch peer priority error");
                    }
                }
                sleep(Duration::from_secs(1)).await;
            }
        }
    }

    pub async fn bill_peer(
        &self,
        peer: u32,
        unit: &str,
        amount: f64,
    ) -> Result<()> {
        trace!(%peer, ?unit, %amount, "AccountClient.bill_peer");
        let mut url = self.server_url.clone();
        url.path_segments_mut().unwrap()
            .push("peer")
            .push(peer.to_string().as_str())
            .push("bill")
            .push(unit);
        let response = self.client
            .post(url)
            .json(&amount)
            .send().await?;
        ensure!(
            response.status().is_success(),
            "error {}",
            response.status(),
        );
        Ok(())
    }

    pub async fn bill_peer_retrying(
        &self,
        peer: u32,
        unit: &str,
        amount: f64,
    ) {
        trace!(%peer, ?unit, %amount, "AccountClient.bill_peer_retring");
        loop {
            match self.bill_peer(peer, unit, amount).await {
                Result::Ok(()) => break,
                Result::Err(e) => {
                    error!(%e, "error billing peer");
                    sleep(Duration::from_secs(1)).await;
                }
            }
        }
    }

    pub async fn credit_peer(
        &self,
        peer: u32,
        unit: &str,
        amount: f64,
    ) -> Result<()> {
        trace!(%peer, ?unit, %amount, "AccountClient.credit_peer");
        let mut url = self.server_url.clone();
        url.path_segments_mut().unwrap()
            .push("peer")
            .push(peer.to_string().as_str())
            .push("credit")
            .push(unit);
        let response = self.client
            .post(url)
            .json(&amount)
            .send().await?;
        ensure!(
            response.status().is_success(),
            "error {}",
            response.status(),
        );
        Ok(())
    }

    pub async fn credit_peer_retrying(
        &self,
        peer: u32,
        unit: &str,
        amount: f64,
    ) {
        trace!(%peer, ?unit, %amount, "AccountClient.credit_peer_retring");
        loop {
            match self.credit_peer(peer, unit, amount).await {
                Result::Ok(()) => break,
                Result::Err(e) => {
                    error!(%e, "error crediting peer");
                    sleep(Duration::from_secs(1)).await;
                }
            }
        }
    }
}
