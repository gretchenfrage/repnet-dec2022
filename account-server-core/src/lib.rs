
#[macro_use]
extern crate tracing;

pub mod units;
pub mod tuning;
pub mod database;


use anyhow::*;
use crate::{
    tuning::TuningParams,
    units::UnitLogic,
    database::DatabaseLogic,
};
use repnet_util::{
    Peer,
    UnitName,
    formula::{
        priority,
        bill,
        credit,
    },
};
use std::{
    collections::hash_map::{
        self,
        HashMap,
    },
    sync::{
        Arc,
        Mutex,
    },
};
use tokio::sync::watch;


#[derive(Clone)]
pub struct AccountServerCore(Arc<State>);

struct State {
    tuning: TuningParams,
    unit_logic: Box<dyn UnitLogic>,
    db_logic: Box<dyn DatabaseLogic>,
    accounts: Mutex<HashMap<Peer, AccountEntry>>,
}

struct AccountEntry {
    tally: f64,
    total: f64,
    priority_watch_send: watch::Sender<f64>,
    priority_watch_recv: watch::Receiver<f64>,
    watch_count: usize,
}

impl AccountEntry {
    fn priority_maybe_changed(&self, trust: f64) {
        let priority = priority(
            self.tally,
            self.total,
            trust,
        );
        if priority != *self.priority_watch_recv.borrow() {
            self.priority_watch_send.send(priority).unwrap();
        }
    }
}

pub struct WatchPriority {
    recv: watch::Receiver<f64>,
    state: Arc<State>,
    peer: Peer,
}

impl WatchPriority {
    pub fn get(&self) -> f64 {
        *self.recv.borrow()
    }

    pub async fn changed(&mut self) -> Result<(), ()> {
        self.recv.changed().await.map_err(drop)
    }
}

impl Clone for WatchPriority {
    fn clone(&self) -> Self {
        {
            let mut accounts = self.state.accounts.lock().unwrap();
            accounts.get_mut(&self.peer).unwrap().watch_count += 1;
        }
        WatchPriority {
            recv: self.recv.clone(),
            state: Arc::clone(&self.state),
            peer: self.peer,
        }
    }
}

impl Drop for WatchPriority {
    fn drop(&mut self) {
        let mut accounts = self.state.accounts.lock().unwrap();
        let mut entry =
            match accounts.entry(self.peer) {
                hash_map::Entry::Occupied(entry) => entry,
                hash_map::Entry::Vacant(_) => unreachable!(),
            };
        entry.get_mut().watch_count -= 1;
        if entry.get().watch_count == 0 {
            entry.remove();
        }
    }
}

impl AccountServerCore {
    pub fn new(
        tuning: TuningParams,
        unit_logic: Box<dyn UnitLogic>,
        db_logic: Box<dyn DatabaseLogic>,
    ) -> Result<Self>
    {
        tuning.validate()?;
        Ok(AccountServerCore(Arc::new(State {
            tuning,
            unit_logic,
            db_logic,
            accounts: Mutex::new(HashMap::new()),
        })))
    }

    pub fn get_priority(&self, peer: Peer) -> f64 {
        let accounts = self.0.accounts.lock().unwrap();
        let (tally, total) = accounts
            .get(&peer)
            .map(|account| (account.tally, account.total))
            .unwrap_or_else(|| self.0.db_logic.get_tally_total(peer));
        priority(
            tally,
            total,
            self.0.tuning.trust,
        )
    }

    pub fn watch_priority(&self, peer: Peer) -> WatchPriority {
        let mut accounts = self.0.accounts.lock().unwrap();
        let recv = accounts
            .entry(peer)
            .and_modify(|account| account.watch_count += 1)
            .or_insert_with(|| {
                let (tally, total) = self.0.db_logic.get_tally_total(peer);
                let priority = priority(
                    tally,
                    total,
                    self.0.tuning.trust,
                );
                let (priority_watch_send, priority_watch_recv) = watch::channel(priority);
                AccountEntry {
                    tally,
                    total,
                    priority_watch_send,
                    priority_watch_recv,
                    watch_count: 1,
                }
            })
            .priority_watch_recv
            .clone();
        WatchPriority {
            recv,
            state: Arc::clone(&self.0),
            peer,
        }
    }

    pub fn bill(&self, peer: Peer, unit: UnitName<&str>, unit_amount: f64) {
        debug_assert!(unit_amount >= 0.0);
        let rep = self.0.unit_logic.to_rep(unit, unit_amount);
        debug_assert!(rep >= 0.0);
        let mut accounts = self.0.accounts.lock().unwrap();
        if let Some(account) = accounts.get_mut(&peer) {
            bill(rep, &mut account.tally, account.total);
            account.priority_maybe_changed(self.0.tuning.trust);
        }
        self.0.db_logic
            .on_bill(
                peer,
                rep,
                unit,
                unit_amount,
            );
    }

    pub fn credit(&self, peer: Peer, unit: UnitName<&str>, unit_amount: f64) {
        debug_assert!(unit_amount >= 0.0);
        let rep = self.0.unit_logic.to_rep(unit, unit_amount);
        debug_assert!(rep >= 0.0);
        let mut accounts = self.0.accounts.lock().unwrap();
        if let Some(account) = accounts.get_mut(&peer) {
            credit(rep, &mut account.tally, &mut account.total);
            account.priority_maybe_changed(self.0.tuning.trust);
        }
        self.0.db_logic
            .on_credit(
                peer,
                rep,
                unit,
                unit_amount,
            );
    }
}
