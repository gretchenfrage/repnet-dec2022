
use anyhow::*;


#[derive(Debug, Clone)]
pub struct TuningParams {
    /// Trust ratio. Cannot be negative.
    pub trust: f64,
}

impl Default for TuningParams {
    fn default() -> Self {
        TuningParams {
            trust: 2.0,
        }
    }
}

impl TuningParams {
    pub fn validate(&self) -> Result<()> {
        ensure!(
            self.trust >= 0.0,
            "negative trust ratio ({})",
            self.trust,
        );
        Ok(())
    }
}
