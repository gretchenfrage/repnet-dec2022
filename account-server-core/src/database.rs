
use crate::Peer;
use repnet_util::{
    UnitName,
    formula::{
        bill,
        credit,
    },
};
use std::{
    collections::HashMap,
    sync::Mutex,
};


pub trait DatabaseLogic: Send + Sync {
    fn get_tally_total(&self, peer: Peer) -> (f64, f64);
    
    /// Add `rep` to peer's tally, unless peer's total is 0. All other params
    /// are extra metadata, optional to use.
    fn on_bill(
        &self,
        peer: Peer,
        rep: f64,
        unit_name: UnitName<&str>,
        unit_amount: f64,
    );

    /// Subtract `rep` from peer's tally and add it to peer's total.
    fn on_credit(
        &self,
        peer: Peer,
        rep: f64,
        unit_name: UnitName<&str>,
        unit_amount: f64,
    );
}


/// For testing and debugging.
#[derive(Debug, Default)]
pub struct HashMapDatabase {
    pub tally_totals: Mutex<HashMap<Peer, (f64, f64)>>,
}

impl DatabaseLogic for HashMapDatabase {
    fn get_tally_total(&self, peer: Peer) -> (f64, f64) {
        let guard = self.tally_totals.lock().unwrap();
        let (
            tally,
            total,
        ) = guard.get(&peer)
            .copied()
            .unwrap_or_default();
        trace!(
            %tally, %total,
            "HashMapDatabase.get_tally_total",
        );
        (tally, total)
    }

    fn on_bill(
        &self,
        peer: Peer,
        rep: f64,
        unit_name: UnitName<&str>,
        unit_amount: f64,
    ) {
        let mut guard = self.tally_totals.lock().unwrap();
        let &mut (
            ref mut tally,
            total,
        ) = guard
            .entry(peer)
            .or_default();
        bill(rep, tally, total);
        trace!(
            %rep,
            %unit_name, %unit_amount,
            %tally, %total,
            "HashMapDatabase.on_bill"
        );
    }

    fn on_credit(
        &self,
        peer: Peer,
        rep: f64,
        unit_name: UnitName<&str>,
        unit_amount: f64,
    ) {
        let mut guard = self.tally_totals.lock().unwrap();
        let &mut (
            ref mut tally,
            ref mut total,
        ) = guard
            .entry(peer)
            .or_default();
        credit(rep, tally, total);

        trace!(
            %rep,
            %unit_name, %unit_amount,
            %tally, %total,
            "HashMapDatabase.on_credit"
        );
    }
}
