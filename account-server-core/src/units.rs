
use std::collections::HashMap;

use repnet_util::units::{
    UNIT_INET_EGRESS_BYTES,
    UNIT_INET_INGRESS_BYTES,
    UnitName,
};


pub trait UnitLogic: Send + Sync {
    fn to_rep(&self, unit: UnitName<&str>, amount: f64) -> f64;
}

#[derive(Debug, Clone)]
pub struct KnownUnits {
    pub weights: HashMap<String, f64>,
}

impl Default for KnownUnits {
    fn default() -> Self {
        let mut weights = HashMap::new();
        weights.insert((&*UNIT_INET_EGRESS_BYTES).to_owned(), 1.0);
        weights.insert((&*UNIT_INET_INGRESS_BYTES).to_owned(), 1.0);
        KnownUnits {
            weights,
        }
    }
}

impl UnitLogic for KnownUnits {
    fn to_rep(&self, unit: UnitName<&str>, amount: f64) -> f64 {
        self.weights
            .get(&*unit)
            .map(|&weight| weight * amount)
            .unwrap_or_else(|| {
                warn!(%unit, %amount, "posted unknown unit");
                0.0
            })
    }
}
