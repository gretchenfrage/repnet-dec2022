
## Repnet Trading API (Prototype)

**Status:** This prototype version is to demonstrate that trading can be done
successfully and with acceptable network usage. **It does not attempt to address
concerns related to cryptography or peer-to-peer transports**, and so, out of
reverence for the potential infohazardousness of a poor attempt at such, it
explicitly and intentionally **avoids even trying**.

We also do not yet attempt to create bayesian or other systems for calibrating
confidence in advertisements. We act as if all advertisements and proposals
made are as earnest as possible all the way down.

### API

We assume that a given peer $n can has a trading server running on their behalf
which can be accessed via this API at `http://127.0.0.1:$n/trading`. Clients
should append the endpoint paths to that URL.

Constants
---

- **SAU** _(Standard Advertisement Unit)_ = 256 * 1024 rep

Endpoints
---

- `/redeem`
    - POST(**give chain**): redeem the promised rep.
      - if the server is not associated with the last `signatory`, error
      - if the current time is later than the last `redeem_by`, reject it
      - if the `ticket_id` has already been redeemed, do not redeem it again,
        but also do not error (be idempotent)
      - to redeem, mark the last `amount` of rep as given to the last `to`
- `/advertise`
    - GET: websocket-upgrade to the advertisement protocol, with the client
      being the advertiser and the server being the listener.

Advertisement Protocol
---

This is an asymmetric protocol over a websocket connection. Messages are
exchanged as JSON text. One side is the **advertiser** and the other side is
the **listener**. It is not request/response-based.

We define a **advertisement**, a JSON tuple-like array with these elements:

```
[
  /* target : peer */,
  /* efficiency : number between 0 and 1 */
]
```

In the context of a given advertisement session, the advertiser has a
hypothetical set of **advertisements**. This set experiences discrete changes
throughout time. For each **advertisement** in the set, the advertiser is
claiming (although not cryptographically guaranteeing) that:

- in exchange for the **listener** giving the **advertiser** (1 **SAU** /
  `efficiency`) rep
- the **advertiser** could get `target` to give a peer of the
  **listener**'s choice 1 **SAU** rep.

Essentially, the bulk of the advertisement protocol is an eventually consistent
data synchronization protocol for the **listener** to maintain a local copy of
some subset of the **advertiser**'s **advertisement set**.

- The **listener** is limited in space and network, so it sets a cap to the
  size of the subset it replicates.
- Thus, if the **listener** is only able to replicate N **advertisements**, it
  wants to replicate the N advertisements _most relevant_ to it.
- Because the **listener** may be making tradeoffs in terms of allocating
  its resource budget between multiple **advertisers** that are advertising
  to it simultaneously, it may also set a _minimum relevance
  threshhold_ on the entries it wants to replicate.
- Finally, since the relevancy of some advertisement to the **listener** may be
  affected by what other information the listener already has, the listener may
  give the **advertiser** feedback on what is relevant to it.

Semantic Signed Declarations
---

NOTE: again, for the proof-of-concept, these are not actually cryptographically
signed at all.

- **give declaration**:
  ```
  {
    "semantics": "give",
    "signatory": /* peer */,

    "to": /* peer */
    "amount": /* non-negative number */,
    "ticket_id": /* uuid string */,
    "redeem_by": /* timestamp */,
  }
  ```
  **Semantic meaning:** By signing this declaration, the signatory declares
  that one may present a **give chain** containing solely this declaration to
  the signatory by `redeem_by` to cause, once, the signatory to mark `amount`
  of the signatory's rep as given to `to`.

- **give-if-get declaration**:
  ```
  {
    "semantics": "give-if-get",
    "signatory": /* peer */,

    "if_get_from": /* peer */,
    "if_get_min_amount": /* amount */,
    "if_get_ticket_id": /* uuid string */,
    "if_get_min_redeem_by": /* timestamp */

    "to": /* peer */
    "amount": /* non-negative number */,
    "ticket_id": /* uuid string */,
    "redeem_by": /* timestamp */,
  }
  ```
  **Semantic meaning:** By signing this declaration, the signatory declares
  that one may present a **give chain** with the last element being this
  declaration to the signatory by `redeem_by` to cause, once, the signatory to
  mark `amount` of the signatory's rep as given to `to`.

- **give-if-get chain**:
  non-empty JSON list of **give-if-get declarations** with the following
  additional requirements:
  - every element's `signatory` equals the next element's `if_get_from`
  - every element's `to` equals the next element's `signatory`
  - every element's `amount` is equal/greater than the next element's
    `if_get_min_amount`
  - every element's `ticket_id` equals the next element's `if_get_ticket_id`
  - every element's `redeem_by` is equal/later than the next element's
    `if_get_min_redeem_by`

- **give chain**:
  JSON list containing 1 **give declaration** followed by any number of
  **give-if-get** declarations, with the exact same additional requirements
  as a **give-if-get chain** (listed above).
