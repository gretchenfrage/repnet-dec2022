
## Repnet Account Server

### API

A client which connects to this account server should be configurable with a
URL. If the URL already has a path, these endpoints should be appended to it.

Endpoints
---

- `/peer/:peer/priority`
    - GET: get current priority.
- `/peer/:peer/priority/watch`
    - GET: websocket-watch priority.
- `/peer/:peer/bill/:unit`
    - POST(json number): report that the peer has consumed the given quantity
      of the given resource on-account. number must be non-negative.
- `/peer/:peer/credit/:unit`
    - POST(json number): report that the peer has provided to self the given
      quantity of the given resource. number must be non-negative.

Data Type Formats
---

- _priority_:
  A JSON number between 0 and 1 (inclusive).
- _peer_:
  For now, this is just a JSON number. Valid values are 32-bit unsigned ints.
  Add cryptography stuff later.
- _unit_:
  `[a-zA-Z0-9-]+`

  Endorsed values (add to as needed):
  - `inet-ingress-bytes`: Number of bytes received from the internet.
  - `inet-egress-bytes`: Number of bytes transmitted into the internet.

Encryption, Authentication, Authorization
---

TODO

Notes
---

- _websocket-watch_:
  Some resources support this way of watching for changes. The client opens a
  websocket connection to the endpoint. The server immediately sends down the
  current value, then sends down further values as the value changes. The
  server may skip some values if it's changing quickly, but should try to keep
  the client up to date without too much lag.

### High-Level Internals Description

The account server maintains a "rep" table:

|peer|tally|total|
|----|-----|-----|

For reference:
- **total** = total value (in rep) peer has provided to self
- **tally** = total value (in rep) self has provided to peer, minus peer has
              provided to self

The account server is configured with a "units" table. Each such known unit
comprises the name of the unit and the unit's "weight", that being the ratio by
which to multiply a quantity of that unit to convert it into a quantity of rep.

By default, the known units are:

|name                |weight|
|--------------------|------|
|`inet-egress-bytes` |1     |
|`inet-ingress-bytes`|1     |

So for example, if one considers egress 5 times as scarce as ingress, they
could reconfigure `inet-ingress-bytes` to weight 0.2. The behavior is
determined only by the ratios between them, but I recommend a norm of setting
`inet-egress-bytes` to 1 so we can have a consistent point of reference when
discussing it.

When the account server receives a bill or credit request, it:
1. Converts the quantity from the given unit into rep using the known units
   units table (ignores if not a known unit).
2. Performs the appropriate incrementation/decrementation of the peer's
   tally/total.

When a peer is billed, the rep is added to the peer's tally. The exception to
this is the _auto-freeloading_ rule: if a peer's total is 0, the account server
doesn't bill it. We do this because, if we didn't, the peers would be
incentivized to freeload when their total is 0 anyways, which would require
them to track it synchronously, which would increase system latency. To avoid
this game-theoretic tragedy, we simply say that conforming servers should do it
automatically upon billing, so it can be asynchronous. And it's not like the
servers lose any expected utility by doing this.

The system is also configured with **trust**, the trust ratio, with a default
of 2.

For a given peer we define **cap** as:

    cap = total * trust

Then we define utility as:

    utility = { if tally <= 0: tally
                if tally >= 0: -cap / ((tally / cap) + 1) + cap }

This creates the piecewise curve we all know and love

![utility curve](other-docs/curves.jpg)

Then we define priority as the slope of that, d-utility / d-tally at the
current tally.

As a special case, when cap = 0, the utility curve is not smooth at tally = 0.
We consider the priority in such cases 0.

As such, we define priority as:

    priority = { if tally <  0: 1
                 if tally >= 0: cap^2 / (tally + cap)^2 }

To save CPU cycles, we may simply have a calculational case of
`if cap = 0 and tally >= 0: 0`.

From here, it's simply a matter of doing reactive programming to keep clients
informed of changing to a peer's priority caused by changes to that peer's rep.
