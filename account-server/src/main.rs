
#[macro_use]
extern crate tracing;


use repnet_util::UnitName;
use repnet_account_server_core::{
    AccountServerCore,
    tuning::TuningParams,
    units::KnownUnits,
    database::HashMapDatabase,
};
use std::{
    env::args,
    net::ToSocketAddrs,
};
use tracing_subscriber::{
    prelude::*,
    Registry,
    EnvFilter,
};
use tokio::task::{
    JoinHandle,
    spawn,
};
use axum::{
    Server,
    Router,
    routing::{
        get,
        post,
    },
    extract::{
        State,
        Path,
        Json,
        ws::{
            self,
            WebSocketUpgrade,
        },
    },
    http::StatusCode,
    response::Response,
};
use futures::{
    stream::StreamExt,
    sink::SinkExt,
};
use anyhow::bail;


fn validate_unit_name(
    name: String,
) -> Result<UnitName<String>, (StatusCode, &'static str)>
{
    UnitName::new(name)
        .map_err(|e| {
            trace!(%e, "invalid unit name");
            (StatusCode::BAD_REQUEST, "invalid unit name")
        })
}

fn validate_non_neg_amount(
    amount: f64,
) -> Result<(), (StatusCode, &'static str)>
{
    if amount < 0.0 {
        return Err((StatusCode::BAD_REQUEST, "negative amount"));
    }
    if !amount.is_finite() {
        return Err((StatusCode::BAD_REQUEST, "non-finite amount"));
    }
    Ok(())
}


async fn get_peer_priority(
    State(core): State<AccountServerCore>,
    Path(peer): Path<u32>,
) -> Json<f64> {
    Json(core.get_priority(peer))
}

async fn watch_peer_priority(
    State(core): State<AccountServerCore>,
    Path(peer): Path<u32>,
    ws: WebSocketUpgrade,
) -> Response {
    let mut watch = core.watch_priority(peer);
    ws
        .on_upgrade(|ws| async move {
            let (mut ws_send, mut ws_recv) = ws.split();
            let handle: JoinHandle<Result<(), ()>> =
                spawn(async move {
                    loop {
                        let msg = ws::Message::Text(watch.get().to_string());
                        ws_send
                            .send(msg).await
                            .map_err(|e| trace!(%e, "ws error"))?;
                        watch.changed().await?;
                    }
                });
            spawn(async move {
                while let Some(Ok(_)) = ws_recv.next().await {}
                handle.abort();
            });
        })
}

async fn bill_peer(
    State(core): State<AccountServerCore>,
    Path((peer, unit)): Path<(u32, String)>,
    Json(amount): Json<f64>,
) -> Result<(), (StatusCode, &'static str)> {
    let unit = validate_unit_name(unit)?;
    validate_non_neg_amount(amount)?;
    core.bill(peer, unit.as_ref(), amount);
    Ok(())
}

async fn credit_peer(
    State(core): State<AccountServerCore>,
    Path((peer, unit)): Path<(u32, String)>,
    Json(amount): Json<f64>,
) -> Result<(), (StatusCode, &'static str)> {
    let unit = validate_unit_name(unit)?;
    validate_non_neg_amount(amount)?;
    core.credit(peer, unit.as_ref(), amount);
    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = args().collect::<Vec<_>>();
    if args.len() != 2 {
        eprintln!("Usage: [binary] [bind to]");
        bail!("invalid args");
    }
    let bind_to = args[1].to_socket_addrs()?.next().unwrap();

    let stdout_log = tracing_subscriber::fmt::layer()
        .pretty();
    let subscriber = Registry::default()
        .with(EnvFilter::from_default_env())
        .with(stdout_log);
    tracing::subscriber::set_global_default(subscriber)
        .expect("unable to install log subscriber");
    
    let tuning = TuningParams::default();
    let unit_logic = KnownUnits::default();
    let db_logic = HashMapDatabase::default();

    let core =
        AccountServerCore::new(
            tuning,
            Box::new(unit_logic) as _,
            Box::new(db_logic) as _,
        )?;

    Server::bind(&bind_to)
        .serve(Router::new()
            .route("/peer/:peer/priority", get(get_peer_priority))
            .route("/peer/:peer/priority/watch", get(watch_peer_priority))
            .route("/peer/:peer/bill/:unit", post(bill_peer))
            .route("/peer/:peer/credit/:unit", post(credit_peer))
            .with_state(core)
            .into_make_service()).await?;

    Ok(())
}
